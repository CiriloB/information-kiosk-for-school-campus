<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = [
            ['category' => 'Scholarship'],
            ['category' => 'Events'],
            ['category' => 'Announcements'],
        ];

        foreach($category as $c){
            Category::create($c);
        }
    }
}
