<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Information extends Model
{
    use HasFactory;
    protected $table = 'informations';
    protected $fillable = [
        'title',
        'slug',
        'featured_image',
        'description',
        'content',
        'user_id',
        'category_id'
    ];

    public function category(){
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function uniqueSlug($title){
        $slug = Str::slug($title, '-');
        $count = Information::where('slug', 'LIKE', "{$slug}%")->count();
        $newCount = $count > 0 ? ++$count : '';
        return $newCount > 0 ? "$slug-$newCount" : $slug;
    }

    public static function updateUniqueSlug($title){
        $slug = Str::slug($title, '-');
        $count = Information::where('slug', 'LIKE', "{$slug}%")->count();
        $newCount = $count > 1 ? ++$count : '';
        return $newCount > 1 ? "$slug-$newCount" : $slug;
    }
    
    public function setSlugAttribute($title){
        return $this->attributes['slug'] = $this->uniqueSlug($title);
     }
    
}
