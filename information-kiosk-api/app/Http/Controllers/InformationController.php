<?php

namespace App\Http\Controllers;

use App\Models\Information;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InformationController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('jwt');
    }
    
    public function index(){
        return User::with(['informations', 'informations.category'])->where('id', Auth::id())->paginate(10);
    }

    public function store(Request $request){
        
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'featured_image' => 'required',
            'content' => 'required',
            'category_id' => 'required'
        ]);

        Information::create([
            'title' => $request->title,
            'slug' => $request->title,
            'featured_image' => $request->featured_image,
            'description' => $request->description,
            'content' => $request->content,
            'user_id' => Auth::id(),
            'category_id' => $request->category_id
        ]);
           
        return response()->json(['msg' => 'Success'], 200);
    }

    
    public function update(Request $request, $id){
        
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'content' => 'required',
            'category_id' => 'required',
            'featured_image' => 'required'
        ]);

        $data = [
            'slug' => Information::updateUniqueSlug($request->title),
            'title' => $request->title,
            'description' => $request->description,
            'content' => $request->content,
            'category_id' => $request->category_id,
            'featured_image' => $request->featured_image,
        ];

        if($request->featuredImage){
            $data['featuredImage'] = $request->featuredImage;
        };

        Information::where('id', $id)->update($data);
    }

    public function destroy($id){
        $info = Information::where('id', $id)->first();
        $this->deleteFileFromServer($info['featuredImage']);
        Information::destroy($id);
    }

    public function deleteFileFromServer($filename){
        $filePath = public_path().'/uploads/'.$filename;
        if(file_exists($filePath)){
            @unlink($filePath);
        }
        return;
    }

    public function uploadFeaturedImage(Request $request){
        $picName = time().'.'.$request->file->extension();
        $request->file->move(public_path('uploads'), $picName);
        return $picName;
    }

    
}
