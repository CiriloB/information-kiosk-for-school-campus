<?php

namespace App\Http\Controllers;

use App\Models\Information;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function getInformations(){
        return Information::with(['category', 'user:id,name'])->paginate(10);
    }

    public function getSingleInfo(Request $request){
        $info = Information::with(['category', 'user:id,name'])->where('slug', $request->slug)->first();
        return response()->json([$info], 200);
    }
}
