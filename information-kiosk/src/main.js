import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Toast from "vue-toastification";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
Vue.config.productionTip = false

import "./assets/css/styles.css";
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import "vue-toastification/dist/index.css";

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(Toast, {
  transition: "Vue-Toastification__bounce",
  maxToasts: 5,
  pauseOnHover: false,
  hideProgressBar: true,
});

//VUE UPLOAD FILE AGENT
import VueFileAgent from 'vue-file-agent';
import 'vue-file-agent/dist/vue-file-agent.css';
Vue.use(VueFileAgent);

new Vue({
  router, store,
  render: h => h(App)
}).$mount('#app')
