import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Auth from "../views/Auth/Auth.vue";
import Dashboard from "../views/Dashboard.vue";
import Info from "../views/Info.vue";
import Create from "../views/Modules/Create.vue";
import Manage from "../views/Modules/Informations.vue";
import Update from "../views/Modules/Update.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/login",
    name: "Authentication",
    component: Auth,
    meta: {
      hasUser: true,
    },
  },
  {
    path: "/:slug",
    name: "information",
    component: Info,
  },
  {
    path: "/info",
    name: "info",
    component: Dashboard,
    children: [
      {
        path: "create",
        name: "create",
        components: {
          create: Create,
        },
      },
      {
        path: "manage",
        name: "manage",
        components: {
          manage: Manage,
        },
      },
      {
        path: "update",
        name: "update",
        components: {
          update: Update,
        },
        props: true,
      },
    ],
    meta: { requiresLogin: true },
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

router.beforeEach((to, from, next) => {
  if (
    to.matched.some((record) => record.meta.requiresLogin) &&
    !localStorage.getItem("auth")
  ) {
    next({ name: "Authentication" });
  } else if (
    to.matched.some((record) => record.meta.hasUser) &&
    localStorage.getItem("auth")
  ) {
    next({ name: "info" });
  } else {
    next();
  }
});

export default router;
