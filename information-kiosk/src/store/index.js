import Vue from "vue";
import Vuex from "vuex";
import AXIOS from "./base/config";

const AUTH = "auth";
const fimage = "uploadFeaturedImage";
const info = "information";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: [],
    token: localStorage.getItem("auth") || "",
    categories: [],
    informations: [],
    homeinfo: [],
    singleinfo: [],
  },
  actions: {
    async SignUpUser({ commit }, data) {
      const res = await AXIOS.post(`${AUTH}/store`, data)
        .then((response) => {
          return response;
        })
        .catch((error) => {
          return error.response;
        });

      return res;
    },
    async LogInUser({ commit }, user) {
      const res = await AXIOS.post(`${AUTH}/login`, user)
        .then((response) => {
          commit("SET_USER", response.data.user);
          commit("SET_TOKEN", response.data.access_token);

          return response;
        })
        .catch((error) => {
          return error.response;
        });

      return res;
    },
    async checkUser({ commit }) {
      const res = await AXIOS.get(
        `${AUTH}/me?token=` + localStorage.getItem("auth")
      )
        .then((response) => {
          commit("SET_USER", response.data);
          commit("SET_TOKEN", localStorage.getItem("auth"));
          return response;
        })
        .catch((error) => {
          return error.response;
        });

      return res;
    },
    async logoutUser({ commit }) {
      const res = await AXIOS.post(`${AUTH}/logout?`)
        .then((response) => {
          commit("UNSET_USER");
          return response;
        })
        .catch((error) => {
          return error.response;
        });

      return res;
    },
    async uploadInformation({ commit }, data) {
      const res = await AXIOS.post(`${info}`, data)
        .then((response) => {
          return response;
        })
        .catch((error) => {
          return error.response;
        });

      return res;
    },
    async updateInformation({ commit }, { data, id }) {
      const res = await AXIOS.put(`${info}/${id}`, data)
        .then((response) => {
          return response;
        })
        .catch((error) => {
          return error.response;
        });

      return res;
    },
    async deleteInfromation({ commit }, id) {
      const res = await AXIOS.delete(`${info}/${id}`)
        .then((response) => {
          return response;
        })
        .catch((error) => {
          return error.response;
        });

      return res;
    },

    async fetchInformations({ commit }) {
      await AXIOS.get(`${info}`)
        .then((response) => {
          commit("SET_INFORMATIONS", response.data.data[0].informations);
        })
        .catch((error) => {
          return error.response;
        });
    },

    async fetchCategories({ commit }) {
      await AXIOS.get("category")
        .then((response) => {
          commit("SET_CATEGORIES", response.data);
        })
        .catch((error) => {
          return error.response;
        });
    },
    async fetchHomeInformations({ commit }) {
      await AXIOS.get("getinfo")
        .then((response) => {
          commit("SET_HOME_INFO", response.data);
        })
        .catch((error) => {
          return error.response;
        });
    },
    async findInfo({ commit }, slug) {
      const res = await AXIOS.get(`singleinfo?slug=${slug}`)
        .then((response) => {
          commit("SET_FOUND_INFO", response.data);
          return response;
        })
        .catch((error) => {
          return error.response;
        });

        return res
    },
  },
  getters: {
    getUser(state) {
      return state.user;
    },
    getToken(state) {
      return state.token;
    },
    getInformations(state) {
      return state.informations;
    },
    getCategories(state) {
      return state.categories;
    },
    getHomeInfo(state) {
      return state.homeinfo;
    },
    getSingePost(state) {
      return state.singleinfo;
    },
  },

  mutations: {
    SET_USER(state, user) {
      state.user = user;
    },
    SET_TOKEN(state, token) {
      localStorage.setItem("auth", token);
      state.token = token;

      const bearer_token = localStorage.getItem("auth") || "";
      AXIOS.defaults.headers.common["Authorization"] = `Bearer ${bearer_token}`;
    },
    UNSET_USER(state) {
      localStorage.removeItem("auth");
      state.token = "";

      AXIOS.defaults.headers.common["Authorization"] = "";
    },
    SET_INFORMATIONS(state, informations) {
      state.informations = informations;
    },
    SET_CATEGORIES(state, categories) {
      state.categories = categories;
    },
    SET_HOME_INFO(state, data) {
      state.homeinfo = data;
    },
    SET_FOUND_INFO(state, data) {
      state.singleinfo = data;
    },
  },
});
